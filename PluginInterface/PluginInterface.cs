// project created on 11.10.2005 at 21:50
using System;

namespace PluginInterface
{
	public interface BaseInterface
	{
		bool Identify(object key);
		object Key
		{
			get;
			set;
		}	
			
	}
	
	public interface TestInterface : BaseInterface 
	{
		void TestMethod();				
	}	
}