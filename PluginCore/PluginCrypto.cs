// created on 13.10.2005 at 12:36
using System.IO;

namespace PluginCore
{
	public class PluginCrypto
	{
		private PluginCrypto()
		{
			   	// create a 1024 bits key pair
     		 	// RSA rsa = RSA.Create ();
     			
     			// convert the key pair into a CryptoAPI PRIVATEKEYBLOB
     			// byte[] keypair = CryptoConvert.ToCapiKeyBlob (rsa, true);
     			
     			// save the key pair to a file
     			//using (FileStream fs = File.OpenWrite ("my.key")) 
     			//{
          		//	fs.Write (keypair, 0, keypair.Length);
     			//}
     			
     			// zeroize the key pair from memory
     			//Array.Clear (keypair, 0, keypair.Length);
     			// note: the private key still exists in the RSA instance!  
		}
		
		private static PluginCrypto fInstance;
		public static PluginCrypto Instance
		{
			get
			{
				if (fInstance == null)
				{
					fInstance = new PluginCrypto();					 
				}
				return fInstance;
			}
		}
		
		public byte [] LoadFile(string fileName)
		{
			FileInfo info = new FileInfo(fileName); 
			StreamReader stream = new StreamReader(fileName);
			BinaryReader reader = new BinaryReader(stream.BaseStream);
			byte [] data = reader.ReadBytes((int)info.Length);
			reader.Close();
			stream.Close();
			return data;
		}		
	}
}