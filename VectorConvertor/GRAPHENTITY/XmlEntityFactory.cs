using System;
using System.Collections;
using System.Xml;
using System.IO;
using VectorConvertor;
using System.Windows.Forms;
using VectorConvertorInterface;

namespace VectorConvertor.GraphEntity
{
	/// <summary>
	/// Summary description for XMLEntity.
	/// </summary>
	public sealed class XmlEntityFactory
	{
		private XmlEntityFactory()
		{			
		}

		private static XmlDocument fXmlDocument;
		private static string fVersion;

		public static IGraphEntity [] LoadData(Stream fileStream)
		{
			StreamReader sr = new StreamReader(fileStream,System.Text.Encoding.UTF8);
			string xmlData = sr.ReadToEnd();
			sr.Close();
			return LoadData(xmlData);
		}

		public static IGraphEntity [] LoadData(string xmlData)
		{
			ArrayList entities = new ArrayList();
			fXmlDocument = new XmlDocument();
			fXmlDocument.LoadXml(xmlData);

			XmlNode vectorConvertorElement = fXmlDocument.FirstChild;
			fVersion = vectorConvertorElement.Attributes["version"].Value;

			foreach (XmlNode elementNode in fXmlDocument.FirstChild.ChildNodes)
			{
				IGraphEntity entity = GetEntityFromXml(elementNode);
				entities.Add(entity);
			}

			return (IGraphEntity [])entities.ToArray(typeof(IGraphEntity));
		}

		public static string Version
		{
			get
			{
				return fVersion;
			}
		}		

		public static void SaveData(string fileName, IGraphEntity [] entities)
		{
			fXmlDocument = new XmlDocument();
			fXmlDocument.AppendChild(fXmlDocument.CreateElement("VectorConvertor"));
			XmlAttribute versionAttr = fXmlDocument.CreateAttribute("version");
			versionAttr.Value = Application.ProductVersion;
			fXmlDocument.FirstChild.Attributes.Append(versionAttr);
			foreach (IGraphEntity entity in entities)
			{
				XmlElement entityElement = fXmlDocument.CreateElement("entity");
				
				XmlAttribute entityTypeAttr = fXmlDocument.CreateAttribute("type");
				entityTypeAttr.Value = entity.GetType().ToString();

				XmlAttribute entityNameAttr = fXmlDocument.CreateAttribute("name");
				entityNameAttr.Value = entity.Name;
				
				XmlAttribute entitySelectedAttr = fXmlDocument.CreateAttribute("selected");
				entitySelectedAttr.Value = entity.Selected.ToString();

				entityElement.Attributes.Append(entityTypeAttr);
				entityElement.Attributes.Append(entityNameAttr);
				entityElement.Attributes.Append(entitySelectedAttr);

				fXmlDocument.FirstChild.AppendChild(entityElement);
				
				XmlElement parametersElement = fXmlDocument.CreateElement("parameters");

				foreach (string key in entity.Parameters.Keys)
				{
					XmlElement parameterElemen = GetXmlForParameter(key,entity.Parameters[key]);
					parametersElement.AppendChild(parameterElemen);
				}

				XmlElement pointsElement = fXmlDocument.CreateElement("points");
				foreach (string key in entity.Points.Keys)
				{
					XmlElement pointElement = fXmlDocument.CreateElement(key);
					XmlAttribute pointNameAttr = fXmlDocument.CreateAttribute("name");
					pointNameAttr.Value = key;
					XmlAttribute pointXAttr = fXmlDocument.CreateAttribute("posX");
					XmlAttribute pointYAttr = fXmlDocument.CreateAttribute("posY");
					pointXAttr.Value = ((System.Drawing.Point)entity.Points[key]).X.ToString();
					pointYAttr.Value = ((System.Drawing.Point)entity.Points[key]).Y.ToString();

					pointElement.Attributes.Append(pointNameAttr);
					pointElement.Attributes.Append(pointXAttr);
					pointElement.Attributes.Append(pointYAttr);
					pointsElement.AppendChild(pointElement);
				}


				entityElement.AppendChild(parametersElement);
				entityElement.AppendChild(pointsElement);
			}

			fXmlDocument.Save(fileName);
		}

		private static XmlElement GetXmlForParameter(string name, object parameter)
		{
			XmlElement parameterElement = fXmlDocument.CreateElement(name);
			XmlAttribute typeAttr = fXmlDocument.CreateAttribute("type");
			typeAttr.Value = parameter.GetType().ToString();

			XmlAttribute colorRAttr = null;
			XmlAttribute colorGAttr = null;
			XmlAttribute colorBAttr = null;
			XmlAttribute widthAttr = null;
			XmlAttribute sizeWAttr = null;
			XmlAttribute sizeHAttr = null;
			XmlAttribute fontFamilyAttr = null;
			XmlAttribute fontSizeAttr = null;
			XmlAttribute textAttr = null;

			
			switch (parameter.GetType().ToString())
			{
				case "System.Drawing.Pen" :
					colorRAttr = fXmlDocument.CreateAttribute("colorR");					
					colorGAttr = fXmlDocument.CreateAttribute("colorG");					
					colorBAttr = fXmlDocument.CreateAttribute("colorB");					
					colorRAttr.Value = ((System.Drawing.Pen)parameter).Color.R.ToString();
					colorGAttr.Value = ((System.Drawing.Pen)parameter).Color.G.ToString();
					colorBAttr.Value = ((System.Drawing.Pen)parameter).Color.B.ToString();
					widthAttr = fXmlDocument.CreateAttribute("width");
					widthAttr.Value = ((System.Drawing.Pen)parameter).Width.ToString();
					break;
				case "System.Drawing.Size" :
					sizeWAttr = fXmlDocument.CreateAttribute("sizeWidth");
					sizeWAttr.Value = ((System.Drawing.Size)parameter).Width.ToString();
					sizeHAttr = fXmlDocument.CreateAttribute("sizeHeight");
					sizeHAttr.Value = ((System.Drawing.Size)parameter).Height.ToString();
					break;
				case "System.Drawing.Font" :					
					fontFamilyAttr = fXmlDocument.CreateAttribute("fontFamily");                    
					fontSizeAttr = fXmlDocument.CreateAttribute("fontSize");                    
					fontFamilyAttr.Value = ((System.Drawing.Font)parameter).FontFamily.Name;
					fontSizeAttr.Value = ((System.Drawing.Font)parameter).Size.ToString();
					break;
				case "System.String" :
					textAttr = fXmlDocument.CreateAttribute("text");
					textAttr.Value = parameter.ToString();
					break;
				default :
					throw new System.InvalidProgramException("Xml parameter fo type " + parameter.GetType().ToString() + " not realized");
			}

			XmlAttribute [] attributes = new XmlAttribute [] {colorRAttr, colorGAttr, colorBAttr, widthAttr, sizeWAttr, sizeHAttr,fontFamilyAttr,fontSizeAttr,textAttr};

			foreach (XmlAttribute attr in attributes)
			{
				if (attr != null)
					parameterElement.Attributes.Append(attr);
			}			
			return parameterElement;
		}

		private static IGraphEntity GetEntityFromXml(XmlNode entityNode)
		{
			IGraphEntity entity = null;
			string entityType = entityNode.Attributes["type"].Value;			
			Hashtable parameters = new Hashtable();
			Hashtable points = new Hashtable();
			switch (entityType)
			{
				case "VectorConvertor.GraphEntity.LineEntity" :
                    entity = new LineEntity();
					break;
				case "VectorConvertor.GraphEntity.RectangleEntity" :
					entity = new RectangleEntity();
					break;
				case "VectorConvertor.GraphEntity.CircleEntity" :
					entity = new CircleEntity();
					break;
				case "VectorConvertor.GraphEntity.PointEntity" :
					entity = new PointEntity();
					break;
				case "VectorConvertor.GraphEntity.CircleFillEntity" :
					entity = new CircleFillEntity();
					break;
				case "VectorConvertor.GraphEntity.RectangleFillEntity" :
					entity = new RectangleFillEntity();
					break;
				case "VectorConvertor.GraphEntity.TextEntity" :
					entity = new TextEntity();
					break;
			}

			XmlNode parametersNode = null;
			XmlNode pointsNode = null;

			int optionsCount =0;
			foreach (XmlNode optionsNode in entityNode.ChildNodes)
			{
				if (optionsNode.Name == "parameters")
				{
					optionsCount++;
					parametersNode = optionsNode;
				}
				if (optionsNode.Name == "points")
				{
					optionsCount++;
					pointsNode = optionsNode;
				}
			}

			if (optionsCount != 2)
				throw new InvalidProgramException("Suported options tags are <parameters> and <points>");
			
			foreach (XmlNode parameterNode in parametersNode.ChildNodes)
			{
				parameters.Add(parameterNode.Name,GetParamenterFormXml(parameterNode));				
			}

			foreach (XmlNode pointNode in pointsNode.ChildNodes)
			{
				points.Add(pointNode.Name,new System.Drawing.Point(Convert.ToInt32(pointNode.Attributes["posX"].Value),Convert.ToInt32(pointNode.Attributes["posY"].Value)));
			}

			entity.Name = entityNode.Attributes["name"].Value;
			entity.Parameters = parameters;
			entity.Points = points;
			entity.Selected = entityNode.Attributes["selected"].Value == "True";

			return entity;
		}

		private static object GetParamenterFormXml(XmlNode parameterNode)
		{
			switch (parameterNode.Name)
			{
				case "Pen":
					return new System.Drawing.Pen(System.Drawing.Color.FromArgb(Convert.ToInt32(parameterNode.Attributes["colorR"].Value),Convert.ToInt32(parameterNode.Attributes["colorG"].Value),Convert.ToInt32(parameterNode.Attributes["colorB"].Value)),Convert.ToInt32(parameterNode.Attributes["width"].Value));
				case "Size" :
					return new System.Drawing.Size(Convert.ToInt32(parameterNode.Attributes["sizeWidth"].Value),Convert.ToInt32(parameterNode.Attributes["sizeHeight"].Value));
				case "Text" :
					return parameterNode.Attributes["text"].Value;
				case "Font" :
					return new System.Drawing.Font(new System.Drawing.FontFamily(parameterNode.Attributes["fontFamily"].Value),(float)Convert.ToDouble(parameterNode.Attributes["fontSize"].Value));
				default :
					throw new InvalidProgramException("Not Supported paramenter "+parameterNode.Name);
			}
		}
	}
}
