using System;
using System.Collections;
using System.Xml;

namespace VectorConvertor.AppConfigLogic
{
	/// <summary>
	/// Summary description for AppConfigProvider.
	/// </summary>
	public class AppConfigProvider
	{
		private string fAppConfigFileName;			
		private XmlDocument fAppConfigXml;
		public AppConfigProvider(string appConfigFileName)
		{			
			fAppConfigFileName = appConfigFileName;
			fAppConfigXml = new XmlDocument();
			fAppConfigXml.Load(appConfigFileName);
		}
		
		private Hashtable fToolbarNames = new Hashtable();
		public Hashtable ToolbarNames
		{
			get
			{
				fToolbarNames.Clear();
				XmlNodeList toolbarsXml = fAppConfigXml.SelectNodes("/configuration/pluginSettings/toolbar");
				
				foreach (XmlNode toolXml in toolbarsXml)
				{
					if (fToolbarNames.Contains(toolXml.Attributes["key"].Value))
						throw new ApplicationException("App config aready contain toolbar " + toolXml.Attributes["key"].Value);
					fToolbarNames.Add(toolXml.Attributes["key"].Value,toolXml.Attributes["file"].Value);
				}
				return fToolbarNames;				
			}
		}
	}
}
