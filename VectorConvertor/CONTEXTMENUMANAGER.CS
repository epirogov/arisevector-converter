using System;
using System.Windows.Forms;

namespace VectorConvertor
{
	/// <summary>
	/// Summary description for ContextMenuManager.
	/// </summary>
	public sealed class ContextMenuManager
	{		
		private ContextMenuManager()
		{			
		}

		private static MenuItem fBgImageItem;
		private static MenuItem fMoveItem;
		private static MenuItem fScaleItem;
		private static MenuItem fRotateItem;

		public static void Initialize()
		{
			fBgImageItem = new MenuItem("Background");					
			fMoveItem = new MenuItem("Move");
			fScaleItem = new MenuItem("Scale");
			fRotateItem = new MenuItem("Rotate");

			fBgImageItem.Click +=new EventHandler(bgImageItem_Click);		
			fMoveItem.Click +=new EventHandler(moveItem_Click);
			fScaleItem.Click +=new EventHandler(scaleItem_Click);
			fRotateItem.Click += new EventHandler(rotateItem_Click);

		}

		private static VCCore fCore;
		public static VCCore Core
		{
			get
			{
				return fCore;
			}

			set
			{
				fCore = value;
			}
		}

		private static OpenFileDialog fOpenFileDialog = new OpenFileDialog();
		public static OpenFileDialog OpenFileDialog
		{
			get
			{
				return fOpenFileDialog;
			}
		}

		public static void ClearContextMenu(string menuName, ContextMenu menu)
		{
			switch (menuName)
			{
				case "MapMenu" :
					if (menu.MenuItems.Contains(fBgImageItem))
					{
						menu.MenuItems.Remove(fBgImageItem);
					}					
					break;
				case "MoveMenu" :
					if (menu.MenuItems.Contains(fMoveItem))
					{
						menu.MenuItems.Remove(fMoveItem);
					}					
					break;
				case "ScaleMenu" :
					if (menu.MenuItems.Contains(fScaleItem))
					{
						menu.MenuItems.Remove(fScaleItem);
					}					
					break;
				case "RotateMenu" :
					if (menu.MenuItems.Contains(fScaleItem))
					{
						menu.MenuItems.Remove(fRotateItem);
					}					
					break;
			}
		}

		public static void ClearContextMenu(ContextMenu menu)
		{
			menu.MenuItems.Clear();
		}


		public static void SetContextMenu(string menuName, ContextMenu menu)
		{            
			switch (menuName)
			{
				case "MapMenu" :
					if (!menu.MenuItems.Contains(fBgImageItem))
					{
						menu.MenuItems.Add(fBgImageItem);
					}					
					break;
				case "MoveMenu" :
					if (!menu.MenuItems.Contains(fMoveItem))
					{
						menu.MenuItems.Add(fMoveItem);
					}					
					break;
				case "ScaleMenu" :
					if (!menu.MenuItems.Contains(fScaleItem))
					{
						menu.MenuItems.Add(fScaleItem);
					}					
					break;
				case "RotateMenu" :
					if (!menu.MenuItems.Contains(fRotateItem))
					{
						menu.MenuItems.Add(fRotateItem);
					}					
					break;
			}
		}

		private static void bgImageItem_Click(object sender, System.EventArgs e)
		{
			if (fCore == null)
				return;
			if (fOpenFileDialog.ShowDialog() == DialogResult.OK)
			{
				System.Drawing.Image img = new System.Drawing.Bitmap(fOpenFileDialog.FileName);
				fCore.ParentControl.Map.BackgroundImage = img;
				fCore.ParentControl.Map.Size = new System.Drawing.Size(img.Width,img.Height);				
				fCore.ParentControl.Map.Show();

			}			
		}

		private static void moveItem_Click(object sender, System.EventArgs e)
		{
			fCore.State = VCCore.CoreState.Move;
			fCore.MoveName =  "Move";
		}

		private static void scaleItem_Click(object sender, System.EventArgs e)
		{
			fCore.State = VCCore.CoreState.Move;
			fCore.MoveName =  "Scale";
		}

		private static void rotateItem_Click(object sender, System.EventArgs e)
		{
			fCore.State = VCCore.CoreState.Move;
			fCore.MoveName =  "Rotate";
		}		
	}
}
