using System;
using System.Drawing;

namespace VectorConvertorInterface
{
	public interface BaseGraphInterface : PluginInterface.BaseInterface
	{
		Bitmap Presentation
		{
			get;
			set;
		}
	}
}