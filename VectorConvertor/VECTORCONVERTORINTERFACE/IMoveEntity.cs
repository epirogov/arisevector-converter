using System;
using System.Collections;

namespace VectorConvertorInterface
{
	/// <summary>
	/// Summary description for IMoveEntity.
	/// </summary>
    public interface IMoveEntity : BaseGraphInterface
	{
		Hashtable Parameters
		{
			get;
			set;
		}
		Hashtable Points
		{
			get;
			set;
		}

		void Create(IGraphEntity entity, Hashtable parameters);		

		IGraphEntity GraphEntity
		{
			get;
			set;
		}

		void Execute();
	}
}
