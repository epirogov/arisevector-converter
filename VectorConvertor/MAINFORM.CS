using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using VectorConvertorInterface;

namespace VectorConvertor
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form, IGrapgicMap
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.TabControl tbTools;
		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.PropertyGrid propertyGrid1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel plMap;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.MenuItem menuItem10;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.Splitter splitter2;
		private System.Windows.Forms.MenuItem menuItem12;
		private System.Windows.Forms.MenuItem menuItem13;
		private System.Windows.Forms.ContextMenu cmMap;
		private System.Windows.Forms.MenuItem menuItem14;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label lbToolName;
		private System.Windows.Forms.Panel panel5;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MainForm()
		{			
			InitializeComponent();
			Init();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.tbTools = new System.Windows.Forms.TabControl();
			this.panel4 = new System.Windows.Forms.Panel();
			this.lbToolName = new System.Windows.Forms.Label();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel3 = new System.Windows.Forms.Panel();
			this.splitter2 = new System.Windows.Forms.Splitter();
			this.plMap = new System.Windows.Forms.Panel();
			this.cmMap = new System.Windows.Forms.ContextMenu();
			this.panel2 = new System.Windows.Forms.Panel();
			this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
			this.mainMenu = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.menuItem12 = new System.Windows.Forms.MenuItem();
			this.menuItem13 = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.menuItem14 = new System.Windows.Forms.MenuItem();
			this.menuItem9 = new System.Windows.Forms.MenuItem();
			this.menuItem11 = new System.Windows.Forms.MenuItem();
			this.menuItem10 = new System.Windows.Forms.MenuItem();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.panel1.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.panel5);
			this.panel1.Controls.Add(this.panel4);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(88, 453);
			this.panel1.TabIndex = 0;
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.tbTools);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel5.Location = new System.Drawing.Point(0, 22);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(88, 431);
			this.panel5.TabIndex = 2;
			// 
			// tbTools
			// 
			this.tbTools.Alignment = System.Windows.Forms.TabAlignment.Left;
			this.tbTools.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbTools.Location = new System.Drawing.Point(0, 0);
			this.tbTools.Multiline = true;
			this.tbTools.Name = "tbTools";
			this.tbTools.SelectedIndex = 0;
			this.tbTools.Size = new System.Drawing.Size(88, 431);
			this.tbTools.TabIndex = 0;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.lbToolName);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(88, 22);
			this.panel4.TabIndex = 1;
			// 
			// lbToolName
			// 
			this.lbToolName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbToolName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.lbToolName.Location = new System.Drawing.Point(0, 0);
			this.lbToolName.Name = "lbToolName";
			this.lbToolName.Size = new System.Drawing.Size(88, 22);
			this.lbToolName.TabIndex = 0;
			this.lbToolName.Text = "[ToolName]";
			this.lbToolName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(88, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 453);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.splitter2);
			this.panel3.Controls.Add(this.plMap);
			this.panel3.Controls.Add(this.panel2);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(91, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(541, 453);
			this.panel3.TabIndex = 2;
			// 
			// splitter2
			// 
			this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
			this.splitter2.Location = new System.Drawing.Point(354, 0);
			this.splitter2.Name = "splitter2";
			this.splitter2.Size = new System.Drawing.Size(3, 453);
			this.splitter2.TabIndex = 2;
			this.splitter2.TabStop = false;
			// 
			// plMap
			// 
			this.plMap.BackColor = System.Drawing.Color.LightYellow;
			this.plMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.plMap.ContextMenu = this.cmMap;
			this.plMap.Dock = System.Windows.Forms.DockStyle.Fill;
			this.plMap.ForeColor = System.Drawing.SystemColors.ControlText;
			this.plMap.Location = new System.Drawing.Point(0, 0);
			this.plMap.Name = "plMap";
			this.plMap.Size = new System.Drawing.Size(357, 453);
			this.plMap.TabIndex = 1;
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.propertyGrid1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel2.Location = new System.Drawing.Point(357, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(184, 453);
			this.panel2.TabIndex = 0;
			// 
			// propertyGrid1
			// 
			this.propertyGrid1.CommandsVisibleIfAvailable = true;
			this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.propertyGrid1.LargeButtons = false;
			this.propertyGrid1.LineColor = System.Drawing.SystemColors.ScrollBar;
			this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
			this.propertyGrid1.Name = "propertyGrid1";
			this.propertyGrid1.Size = new System.Drawing.Size(182, 451);
			this.propertyGrid1.TabIndex = 0;
			this.propertyGrid1.Text = "propertyGrid1";
			this.propertyGrid1.ViewBackColor = System.Drawing.SystemColors.Window;
			this.propertyGrid1.ViewForeColor = System.Drawing.SystemColors.WindowText;
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuItem1,
																					 this.menuItem12,
																					 this.menuItem7,
																					 this.menuItem9});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem2,
																					  this.menuItem3,
																					  this.menuItem4,
																					  this.menuItem5,
																					  this.menuItem6});
			this.menuItem1.Text = "File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "New";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "Open";
			this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 2;
			this.menuItem4.Text = "Save";
			this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 3;
			this.menuItem5.Text = "Save As";
			this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 4;
			this.menuItem6.Text = "Close";
			this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
			// 
			// menuItem12
			// 
			this.menuItem12.Index = 1;
			this.menuItem12.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem13});
			this.menuItem12.Text = "View";
			// 
			// menuItem13
			// 
			this.menuItem13.Index = 0;
			this.menuItem13.Text = "Object Tree";
			this.menuItem13.Click += new System.EventHandler(this.menuItem13_Click);
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 2;
			this.menuItem7.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem8,
																					  this.menuItem14});
			this.menuItem7.Text = "Generate";
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 0;
			this.menuItem8.Text = "Run";
			this.menuItem8.Click += new System.EventHandler(this.menuItem8_Click);
			// 
			// menuItem14
			// 
			this.menuItem14.Enabled = false;
			this.menuItem14.Index = 1;
			this.menuItem14.Text = "Options";
			this.menuItem14.Click += new System.EventHandler(this.menuItem14_Click);
			// 
			// menuItem9
			// 
			this.menuItem9.Index = 3;
			this.menuItem9.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem11,
																					  this.menuItem10});
			this.menuItem9.Text = "Help";
			// 
			// menuItem11
			// 
			this.menuItem11.Enabled = false;
			this.menuItem11.Index = 0;
			this.menuItem11.Text = "Develop HandBook";
			// 
			// menuItem10
			// 
			this.menuItem10.Index = 1;
			this.menuItem10.Text = "About";
			this.menuItem10.Click += new System.EventHandler(this.menuItem10_Click);
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(632, 453);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Menu = this.mainMenu;
			this.Name = "MainForm";
			this.Text = "Vector Convertor";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.panel1.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());			
		}

		private VCCore core;		
		
		private void Init()
		{
			core = new VCCore(this);
			PluggableLogic.ToolManager.Instance.ParentTab = tbTools;						
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			// new
			core.FileName = null;
			core.Clear();
			this.plMap.Refresh();
		}

		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void menuItem5_Click(object sender, System.EventArgs e)
		{
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				string fileName = saveFileDialog.FileName;
				core.Save(fileName);
			}
		}

		private void toolControl1_Load(object sender, System.EventArgs e)
		{
		
		}

		private void menuItem8_Click(object sender, System.EventArgs e)
		{
			if(saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				core.GenerateCode(saveFileDialog.FileName);
			}
		}

		private void menuItem13_Click(object sender, System.EventArgs e)
		{
			//Object Tree
			core.ViewObjectTree();			
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			//Open file
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				core.Load(openFileDialog.FileName);
			}
		}
		
		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			if (core.FileName != null) 
			{
				core.Load(core.FileName);
			}
			else
			{
				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					core.Load(openFileDialog.FileName);
				}
			}
		}

		private void menuItem10_Click(object sender, System.EventArgs e)
		{
			//About
			AboutForm aboutForm = new AboutForm();
			aboutForm.ShowDialog();
		}

		private void menuItem14_Click(object sender, System.EventArgs e)
		{
			//Generate Options
			core.ShowGenerateOptionsForm();
		}

		private void MainForm_Load(object sender, System.EventArgs e)
		{
			core.AppConfigFileName = "./VectorConvertor.exe.config";
			core.InitPlugins();
		}			

		public object SelectedObject
		{
			get
			{
				return propertyGrid1.SelectedObject;
			}
			set
			{
				propertyGrid1.SelectedObject = value;
			}
		}

		public Control Map
		{
			get
			{
				return plMap;
			}
			set
			{
				
			}
		}

		public ContextMenu MapMenu
		{
			get
			{
				return cmMap;
			}
			set
			{
				cmMap = value;
			}
		}

		public string ToolName
		{
			get
			{
				return lbToolName.Text;
			}
			set
			{
				lbToolName.Text = value;
			}
		}
	}
}
