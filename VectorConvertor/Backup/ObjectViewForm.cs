using System;
using System.Collections;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using VectorConvertor.GraphEntity;
using VectorConvertorInterface;

namespace VectorConvertor
{	
	/// <summary>
	/// Summary description for ObjectViewForm.
	/// </summary>
	
	[ComVisible(false)]
	public class ObjectViewForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PropertyGrid propertyGrid1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label lbName;
		private System.Windows.Forms.TextBox tbSearch;
		private System.Windows.Forms.Button btnGo;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ObjectViewForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ObjectViewForm));
			this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnGo = new System.Windows.Forms.Button();
			this.tbSearch = new System.Windows.Forms.TextBox();
			this.lbName = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel3 = new System.Windows.Forms.Panel();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// propertyGrid1
			// 
			this.propertyGrid1.CommandsVisibleIfAvailable = true;
			this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.propertyGrid1.LargeButtons = false;
			this.propertyGrid1.LineColor = System.Drawing.SystemColors.ScrollBar;
			this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
			this.propertyGrid1.Name = "propertyGrid1";
			this.propertyGrid1.Size = new System.Drawing.Size(292, 250);
			this.propertyGrid1.TabIndex = 0;
			this.propertyGrid1.Text = "propertyGrid1";
			this.propertyGrid1.ViewBackColor = System.Drawing.SystemColors.Window;
			this.propertyGrid1.ViewForeColor = System.Drawing.SystemColors.WindowText;
			this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnGo);
			this.panel1.Controls.Add(this.tbSearch);
			this.panel1.Controls.Add(this.lbName);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(292, 40);
			this.panel1.TabIndex = 1;
			// 
			// btnGo
			// 
			this.btnGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnGo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnGo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnGo.Location = new System.Drawing.Point(264, 16);
			this.btnGo.Name = "btnGo";
			this.btnGo.Size = new System.Drawing.Size(26, 20);
			this.btnGo.TabIndex = 2;
			this.btnGo.Text = "Go";
			this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
			// 
			// tbSearch
			// 
			this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.tbSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbSearch.Location = new System.Drawing.Point(176, 16);
			this.tbSearch.Name = "tbSearch";
			this.tbSearch.Size = new System.Drawing.Size(88, 20);
			this.tbSearch.TabIndex = 1;
			this.tbSearch.Text = "";
			this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
			// 
			// lbName
			// 
			this.lbName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbName.ForeColor = System.Drawing.SystemColors.Highlight;
			this.lbName.Location = new System.Drawing.Point(8, 8);
			this.lbName.Name = "lbName";
			this.lbName.Size = new System.Drawing.Size(168, 30);
			this.lbName.TabIndex = 0;
			this.lbName.Text = "[Name]";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel4);
			this.panel2.Controls.Add(this.splitter1);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 40);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(292, 437);
			this.panel2.TabIndex = 2;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.propertyGrid1);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(0, 187);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(292, 250);
			this.panel4.TabIndex = 3;
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter1.Location = new System.Drawing.Point(0, 184);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(292, 3);
			this.splitter1.TabIndex = 2;
			this.splitter1.TabStop = false;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.treeView1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(292, 184);
			this.panel3.TabIndex = 1;
			// 
			// treeView1
			// 
			this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView1.ImageIndex = -1;
			this.treeView1.Location = new System.Drawing.Point(0, 0);
			this.treeView1.Name = "treeView1";
			this.treeView1.SelectedImageIndex = -1;
			this.treeView1.Size = new System.Drawing.Size(292, 184);
			this.treeView1.TabIndex = 0;
			this.treeView1.Click += new System.EventHandler(this.treeView1_Click);
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
			// 
			// ObjectViewForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 477);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ObjectViewForm";
			this.Text = "Object Tree";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.ObjectViewForm_Closing);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public event VCCore.ObjectClick ObjectClickEvent;
		private void treeView1_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if (fGraphStorage.Figures.Contains(e.Node.Text))
			{
				lbName.Text = ((IGraphEntity)fGraphStorage.Figures[e.Node.Text]).Name;
				propertyGrid1.SelectedObject = fGraphStorage.Figures[e.Node.Text];
				if (ObjectClickEvent != null)
				{
					ObjectClickEvent(e.Node.Text);
				}
			}
		}

		public TreeView ObjectTree
		{
			get
			{
				return treeView1;
			}
		}


		private GraphStorage fGraphStorage;

		private void propertyGrid1_PropertyValueChanged(object s, System.Windows.Forms.PropertyValueChangedEventArgs e)
		{
			if (treeView1.SelectedNode != null && e.ChangedItem.Label == "Name")
			{
				fGraphStorage.RenameFigure(treeView1.SelectedNode.Text,e.ChangedItem.Value.ToString());				
				treeView1.SelectedNode.Text = e.ChangedItem.Value.ToString();				
			}						
		}

		private void treeView1_Click(object sender, System.EventArgs e)
		{
			if (treeView1.SelectedNode != null && fGraphStorage.Figures.Contains(treeView1.SelectedNode.Text))
			{				
				if (ObjectClickEvent != null)
				{
					ObjectClickEvent(treeView1.SelectedNode.Text);
				}
			}		
		}

		private void ObjectViewForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			this.Hide();
			e.Cancel = true;
		}

		private TreeNode fSelectedNode;
		private void btnGo_Click(object sender, System.EventArgs e)
		{
			treeView1.ExpandAll();
			TreeNode newSelectedNode = null;
			bool foundSelectedNode = fSelectedNode == null;
			foreach (TreeNode node in treeView1.Nodes[0].Nodes)
			{					
				if (newSelectedNode == null && node.Text.IndexOf(tbSearch.Text) != -1 && foundSelectedNode)
				{				
					newSelectedNode = node;
					treeView1.SelectedNode = node;					
					treeView1.Focus();
				}
				else
					node.Collapse();				
				if(fSelectedNode ==  node)
					foundSelectedNode = true;
			}
			fSelectedNode = newSelectedNode;
		}

		private void tbSearch_TextChanged(object sender, System.EventArgs e)
		{
			fSelectedNode = null;
		}
		
		public GraphStorage GraphStorage
		{
			get
			{
				return fGraphStorage;
			}

			set
			{
				fGraphStorage = value;
			}			
		}
	}
}
