using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace VectorConvertor
{
	/// <summary>
	/// Summary description for ToolControl.
	/// </summary>
	    
	[ComVisible(false)]
	public class ToolControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.PictureBox picToolImage;
		private System.Windows.Forms.Panel panel1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ToolControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.picToolImage = new System.Windows.Forms.PictureBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// picToolImage
			// 
			this.picToolImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.picToolImage.Location = new System.Drawing.Point(8, 8);
			this.picToolImage.Name = "picToolImage";
			this.picToolImage.Size = new System.Drawing.Size(32, 32);
			this.picToolImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.picToolImage.TabIndex = 0;
			this.picToolImage.TabStop = false;
			this.picToolImage.Click += new System.EventHandler(this.picToolImage_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.picToolImage);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(50, 50);
			this.panel1.TabIndex = 1;
			// 
			// ToolControl
			// 
			this.Controls.Add(this.panel1);
			this.Name = "ToolControl";
			this.Size = new System.Drawing.Size(50, 50);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public event VCCore.ToolClick ToolClickEvent;
		private void picToolImage_Click(object sender, System.EventArgs e)
		{
			Checked = !Checked;
			if (ToolClickEvent != null)
                ToolClickEvent(this);			
		}

		public string fToolName;
		[Browsable(true)]
		public string ToolName
		{
			get
			{
				return fToolName;
			}

			set
			{
				fToolName = value;				
			}
		}

		public string fImageFilName;
		
	
		[Browsable(true)]
		public string ImageFilName
		{
			get
			{
				return fImageFilName;
			}

			set
			{
				fImageFilName = value;
				Bitmap img = new Bitmap(fImageFilName);
				picToolImage.Image = img;
			}
		}

		private Bitmap fImage;
		public Bitmap Image
		{
			get
			{
				return fImage;
			}

			set
			{
				fImage = value;				
				picToolImage.Image = fImage;
			}
		}

		[Browsable(true)]
		private bool fChecked;

		public bool Checked
		{
			get
			{
				return fChecked;
			}

			set
			{
				fChecked = value;
				if (fChecked)
				{
					panel1.BorderStyle = BorderStyle.FixedSingle;
				}
				else
				{
					panel1.BorderStyle = BorderStyle.None;
				}
			}
		}

		public string fPluginName;
		public string PluginName
		{
			get
			{
				return fPluginName;
			}
			set
			{
				fPluginName = value;
			}
		}
	}
}
