using System;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
using VectorConvertor.GraphEntity;
using VectorConvertor.MoveLogic;
using VectorConvertorInterface;
using VectorConvertor.AppConfigLogic;

namespace VectorConvertor
{
	/// <summary>
	/// Summary description for VCCore.
	/// </summary>
	public class VCCore : IDisposable
	{
		public delegate void ToolClick(object sender);	
		public delegate void ObjectClick(string objectKey);

		private IGrapgicMap fParentControl;
		public IGrapgicMap ParentControl
		{
			get
			{
				return fParentControl;
			}
		}

		public VCCore(IGrapgicMap parent)
		{
			fParentControl = parent;			
			Init();
		}	

		private void Init()
		{		
			fParentControl.ToolName = "";
			fParentControl.Map.Resize +=new EventHandler(Map_Resize);
			fParentControl.Map.Click += new EventHandler(Map_Click);
			fParentControl.Map.MouseDown += new MouseEventHandler(Map_MouseDown);
			fParentControl.Map.MouseMove += new MouseEventHandler(Map_MouseMove);
			fParentControl.Map.MouseUp += new MouseEventHandler(Map_MouseUp);
			fParentControl.Map.Paint +=new PaintEventHandler(Map_Paint);
			fGraphStorage = new GraphStorage(fParentControl.Map.CreateGraphics());
			InitMenu();
			InitObjectView();
		}

		private void InitMenu()
		{
			ContextMenuManager.Initialize();
			ContextMenuManager.Core = this;
			ContextMenuManager.SetContextMenu("MapMenu",fParentControl.MapMenu);
		}

		private void InitObjectView()
		{
			fObjectViewForm.ObjectClickEvent +=new ObjectClick(ObjectClick_Event);
		}		

		private ToolControl fPaintTool;
		public ToolControl PaintTool
		{
			get
			{
				return fPaintTool;
			}
			set
			{
				fPaintTool = value;
			}
		}

		private string fMoveName;
		public string MoveName
		{
			get
			{
				return fMoveName;
			}
			set
			{
				fMoveName = value;
			}
		}

		private Pen fCurrentPen;
		private Graphics fCurrentGraphics;
		private ArrayList fPoints;

		public void ToolClick_Event(object sender)
		{
			fPaintTool = ((ToolControl)sender);	
						
			fCurrentPen = new Pen(Color.Black,1);
			fParentControl.SelectedObject = fCurrentPen;
			InitializeGraphics();
			fPoints = new ArrayList();
			fParentControl.ToolName = fPaintTool.ToolName;
			if (((ToolControl)sender).Checked)
				this.State = CoreState.Paint;
			else
				this.State = CoreState.Select;
		}

		public void InitializeGraphics()
		{
			fCurrentGraphics = fParentControl.Map.CreateGraphics();
		}
		
		private void Map_Resize(object sender, System.EventArgs e)
		{
			
			InitializeGraphics();			
		}

		private void Map_Click(object sender, System.EventArgs e)
		{
		
		}		

        private GraphStorage fGraphStorage;
		private void Map_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			switch (fCoreState)
			{
				case CoreState.Select :
					break;
				case CoreState.Paint :
					Map_MouseDownFor_Paint(sender,e);
					break;
				case CoreState.Move :
					switch (MoveName)
					{
						case "Move" :
							Map_MouseDownFor_Move(sender,e);					
							break;
						case "Scale" :
							Map_MouseDownFor_Scale(sender,e);
							break;
						case "Rotate" :
							Map_MouseDownFor_Rotate(sender,e);
							break;
						default :
							throw new InvalidProgramException(" No Map_MouseDown handler for " + MoveName);
					}
					break;
				default :
					throw new InvalidProgramException("'No find actions for Map_MouseDown with satate " + fCoreState.ToString());
			}
		}

		private void Map_MouseDownFor_Paint(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (fCurrentPen == null)
				return;

			if (e.Button == MouseButtons.Right)
			{
				fPoints = new ArrayList();
				return;
			}

			if (fPoints == null)
				fPoints = new ArrayList();
			Point point = new Point(e.X,e.Y);
			fPoints.Add(point);
			
			PluginCore.PluginSearch.Instance.AssemblyName = fPaintTool.PluginName;			
			try
			{
				IGraphEntity entity = (IGraphEntity)PluginCore.PluginFactory.Instance.GetPlugin(fPaintTool.ToolName);
			
			
			
				if (entity == null)
					throw new InvalidProgramException("Graph Entity " + fPaintTool.ToolName + " for plugin " + fPaintTool.PluginName + " is empty");
				if (fPoints.Count % entity.PointsCountComplite == 0 && fPoints.Count > 0)
				{	
					entity.Parameters.Add("Pen",(Pen)fCurrentPen.Clone());
					entity.SetPointSequence(fPoints);					
					fGraphStorage.AddGraphics(entity);
				}
			}
			catch (CancelActionException)
			{
				fPoints = new ArrayList();
				return;
			}

			fParentControl.Map.Refresh();
			RefreshObjectViewForm();
		}

		private void Map_MouseDownFor_Move(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
				return;
			Point point = new Point(e.X,e.Y);
			if (fSelectedObject != null && fSelectedObject.Selected)
			{
				Hashtable parameters = new Hashtable();
				parameters.Add("Position",point);
				MoveEntity.Instance.Create(fSelectedObject,parameters);
				MoveEntity.Instance.Execute();
				fParentControl.Map.Refresh();
			}
		}

		private void Map_MouseDownFor_Scale(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
				return;
			Point point = new Point(e.X,e.Y);
			if (fSelectedObject != null && fSelectedObject.Selected)
			{
				Hashtable parameters = new Hashtable();
				parameters.Add("Position",point);
				ScaleEntity.Instance.Create(fSelectedObject,parameters);
				ScaleEntity.Instance.Execute();
				fParentControl.Map.Refresh();
			}
		}	
	
		private void Map_MouseDownFor_Rotate(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
				return;
			Point point = new Point(e.X,e.Y);
			if (fSelectedObject != null && fSelectedObject.Selected)
			{
				Hashtable parameters = new Hashtable();
				parameters.Add("Position",point);
				RotateEntity.Instance.Create(fSelectedObject,parameters);
				RotateEntity.Instance.Execute();
				fParentControl.Map.Refresh();
			}
		}			

		private void Map_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
		
		}

		private void Map_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
		
		}

		private void Map_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			fGraphStorage.PrintAll();			
		}	

		public void Save(string fileName)
		{
			fGraphStorage.Save(fileName);
		}

		public void GenerateCode(string fileName)
		{
			fGraphStorage.GenerateCsCode(fileName);
		}

		private ObjectViewForm fObjectViewForm = new ObjectViewForm();
		public void ViewObjectTree()
		{
			if (!fObjectViewForm.Visible)
			{				
				fObjectViewForm.ObjectTree.ExpandAll();				
				RefreshObjectViewForm();
				fObjectViewForm.Show();
			}
			else
			{
				fObjectViewForm.Select();
			}
		}	
	
		public void RefreshObjectViewForm()
		{
			fObjectViewForm.ObjectTree.Nodes.Clear();
			TreeNode rootNode = new TreeNode("Objects");
			fObjectViewForm.ObjectTree.Nodes.Add(rootNode);
			foreach (string key in fGraphStorage.PaintSequence)
			{
				TreeNode newNode = new TreeNode(key);				
				rootNode.Nodes.Add(newNode);
			}
			fObjectViewForm.GraphStorage = fGraphStorage;
		}

		private IGraphEntity fSelectedObject;
		public IGraphEntity SelectedObject
		{
			get
			{
				return fSelectedObject;
			}
		}

		private void ObjectClick_Event(string objectKey)
		{
			if (fSelectedObject != null && (IGraphEntity)fGraphStorage.Figures[objectKey] != fSelectedObject)
				fSelectedObject.Selected = false;
			fSelectedObject = (IGraphEntity)fGraphStorage.Figures[objectKey];			
			fSelectedObject.Selected = !fSelectedObject.Selected;			
			fParentControl.Map.Refresh();
			State = CoreState.Select;
		}

		public void Load(string fileName)
		{
			fGraphStorage.Load(fileName);
			fParentControl.Map.Refresh();
		}

		public void Clear()
		{
			fGraphStorage.Figures = new Hashtable();
			fGraphStorage.PaintSequence = new ArrayList();
		}

		public string fFileName;
		public string FileName
		{
			get
			{
				return fFileName;
			}
			set
			{
				fFileName = value;
			}
		}

		private GenerateOptionsForm generateOptionsForm = new GenerateOptionsForm();
		public void ShowGenerateOptionsForm()
		{
			if (!generateOptionsForm.Visible)
			{
				generateOptionsForm = new GenerateOptionsForm();
			}
			generateOptionsForm.Show();			
			generateOptionsForm.Select();
		}

		public enum CoreState {Select, Paint, Move};

		public CoreState fCoreState;
		public CoreState State
		{
			get
			{
				return fCoreState;
			}

			set
			{				

				fCoreState = value;
				ContextMenuManager.ClearContextMenu(fParentControl.MapMenu);
				switch (fCoreState)
				{
					case CoreState.Select :
						fParentControl.SelectedObject = null;
						ContextMenuManager.SetContextMenu("MapMenu",fParentControl.MapMenu);
						if (fSelectedObject != null && fSelectedObject.Selected)
						{
							ContextMenuManager.SetContextMenu("MoveMenu",fParentControl.MapMenu);
							ContextMenuManager.SetContextMenu("ScaleMenu",fParentControl.MapMenu);
							ContextMenuManager.SetContextMenu("RotateMenu",fParentControl.MapMenu);
						}
						break;
					case CoreState.Paint :
						break;
					case CoreState.Move :
						ContextMenuManager.SetContextMenu("MoveMenu",fParentControl.MapMenu);
						ContextMenuManager.SetContextMenu("ScaleMenu",fParentControl.MapMenu);
						ContextMenuManager.SetContextMenu("RotateMenu",fParentControl.MapMenu);
						break;					
					default :
						throw new InvalidProgramException("Undefined core state " + fCoreState.ToString());
				}
			}
		}

		private string fAppConfigFileName = "./VectorConvertor.exe.config";
		public string AppConfigFileName
		{
			get
			{
				return fAppConfigFileName;
			}
			set
			{
				fAppConfigFileName = value;
			}
		}

		public void InitPlugins()
		{
			AppConfigProvider configProvider = new AppConfigProvider(fAppConfigFileName);
			Hashtable toolbars = configProvider.ToolbarNames;			
			
			foreach (string toolbarName in toolbars.Keys)
			{
				PluggableLogic.ToolManager.Instance.LoadToolbarPlugins((string)toolbars[toolbarName],toolbarName, this);
			}
		}
		#region IDisposable Members

		public void Dispose()
		{
			// TODO:  Add VCCore.Dispose implementation
		}

		#endregion
	}
}
