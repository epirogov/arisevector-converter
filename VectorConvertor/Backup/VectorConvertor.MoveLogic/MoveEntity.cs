using System;
using System.Drawing;
using System.Collections;
using VectorConvertorInterface;

namespace VectorConvertor.MoveLogic
{
	/// <summary>
	/// Summary description for MoveEntity.
	/// </summary>
	public class MoveEntity : IMoveEntity
	{
		private static MoveEntity fInstance;
		private MoveEntity()
		{			
		}

		public static MoveEntity Instance
		{
			get
			{
				if (fInstance == null)
					fInstance =  new MoveEntity();
				return fInstance;
			}			
		}	
	
		#region IMoveEntity Members

		private System.Collections.Hashtable fParameters;
		public System.Collections.Hashtable Parameters
		{
			get
			{				
				return fParameters;
			}
			set
			{
				fParameters = value;
			}
		}

		private System.Collections.Hashtable fPoints;
		public System.Collections.Hashtable Points
		{
			get
			{				
				return fPoints;
			}
			set
			{
				fPoints = value;
			}
		}


		public void Create(IGraphEntity entity, Hashtable parameters)
		{			
			if (!parameters.Contains("Position"))
				throw new ArgumentException("Parameters not contain key 'Position'");
			fParameters = parameters;
			fGraphEntity = entity;
		}

		private IGraphEntity fGraphEntity;
		public IGraphEntity GraphEntity
		{
			get
			{
				return fGraphEntity;
			}
			set
			{
				fGraphEntity = value;
			}
		}

		public void Execute()
		{
			MathGeometry.SetNewCentre(fGraphEntity,((System.Drawing.Point)fParameters["Position"]));
		}

		#endregion

		#region BaseInterface Members
		
		public bool Identify(object key)
		{
			return Key.Equals(key);
		}
		public object Key
		{
			get
			{
				return "Move Entity";
			}
			set
			{
			}
		}
	
		public Bitmap Presentation
		{
			get
			{
				return PresentationManager.Instance.GetPresentation("RECT.BMP");
			}
			set
			{
			}
		}

		#endregion
	}
}
