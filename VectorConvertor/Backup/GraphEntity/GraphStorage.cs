using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using VectorConvertorInterface;

namespace VectorConvertor.GraphEntity
{
	/// <summary>
	/// Summary description for GraphStorage.
	/// </summary>
	public class GraphStorage
	{
		public GraphStorage()
		{			
		}

		
		private Hashtable fFigures = new Hashtable();
		public Hashtable Figures
		{
			get
			{
				return fFigures;
			}
			set
			{
				fFigures = value;
			}
		}
		
		private Graphics fGraphics;
		public GraphStorage(Graphics grapthics)
		{
			fGraphics = grapthics;
		}

		public void AddGraphics(IGraphEntity entity)
		{
			entity.Name = NewNameForEntity(entity.Key.ToString());
			fFigures.Add(entity.Name,entity);
			fPaintSequence.Add(entity.Name);
		}

		public string NewNameForEntity(string prefix)
		{
			for (int i = 1; i < int.MaxValue; i++)
			{
				if (!fFigures.Contains(prefix+i.ToString()))
				{
					return prefix+i.ToString();
				}
			}
			throw new System.InvalidOperationException("No auto set name for " + prefix);
		}

		public void PrintAll()
		{
			foreach (string key in fPaintSequence)
			{
				IGraphEntity entity = (IGraphEntity)fFigures[key];
				entity.Print(fGraphics);
				fGraphics.Save();
			}
		}		

		public void GenerateCsCode(string fileName)
		{
			CsCodeGenerator codeGenerator = new CsCodeGenerator();
			foreach (string key in fPaintSequence)
			{
				IGraphEntity entity = (IGraphEntity)fFigures[key];
				codeGenerator.AddPaintExpressions(entity.GetInvokation());
			}
			codeGenerator.Generate(fileName);
		}

		public void Save(string fileName)
		{
			IGraphEntity [] entities = new IGraphEntity[fFigures.Count];
			int i =0;
			foreach (string key in fPaintSequence)
			{
				entities[i] =(IGraphEntity)fFigures[key];
				i++;
			}
			XmlEntityFactory.SaveData(fileName,entities);
		}

		public void Load(string fileName)
		{
			System.IO.StreamReader sr = new StreamReader(fileName);
			string xmlData = sr.ReadToEnd();
			sr.Close();
			IGraphEntity [] entities = XmlEntityFactory.LoadData(xmlData);
			if (fPaintSequence == null)
				fPaintSequence = new ArrayList();
			if (fFigures == null)
				fFigures = new Hashtable();

			fPaintSequence.Clear();
			fFigures.Clear();
			if (fGraphics != null)
                fGraphics.Clear(System.Drawing.Color.LightYellow);
			foreach (IGraphEntity entity in entities)
			{			
				fPaintSequence.Add(entity.Name);
				fFigures.Add(entity.Name,entity);
			}
		}
		
		private ArrayList fPaintSequence = new ArrayList();
		public ArrayList PaintSequence 
		{
			get
			{
				return fPaintSequence;
			}
			set
			{
				fPaintSequence = value;
			}
		}
		
		public void RenameFigure(string oldKey, string newKey)
		{
			if (!fFigures.Contains(oldKey))
			{
				return;
			}
			IGraphEntity figure = ((IGraphEntity)fFigures[oldKey]);
			fFigures.Remove(oldKey);
			fFigures.Add(newKey,figure);
			for (int i =0; i < fPaintSequence.Count; i++)
			{
				string key = (string)fPaintSequence[i];
				if (key==oldKey)
				{
					fPaintSequence[i] = newKey;
				}
			}
		}
	}
}
