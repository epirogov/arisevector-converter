using System;
using System.IO;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.CSharp;

namespace VectorConvertor
{
	/// <summary>
	/// Summary description for CsCodeGenerator.
	/// </summary>
	public class CsCodeGenerator
	{
		private CodeCompileUnit compileUnit = new CodeCompileUnit();
		private CodeMemberMethod paintMethod = new CodeMemberMethod();

		public CsCodeGenerator()
		{			
			CodeNamespace codeNamespace = new CodeNamespace("GameUserComponent");
			
			codeNamespace.Imports.Add( new CodeNamespaceImport("System"));
			codeNamespace.Imports.Add( new CodeNamespaceImport("System.ComponentModel"));
			codeNamespace.Imports.Add( new CodeNamespaceImport("System.Drawing"));
			codeNamespace.Imports.Add( new CodeNamespaceImport("System.Windows.Forms"));

			compileUnit.Namespaces.Add(codeNamespace);

			CodeTypeDeclaration componentClass = new CodeTypeDeclaration("GameControlClass");
			componentClass.BaseTypes.Add(typeof(System.Windows.Forms.UserControl));

			//components
			CodeMemberField componentsField = new CodeMemberField(typeof(System.ComponentModel.Container),"components");
			componentsField.Attributes = MemberAttributes.Private;

			//graphics
			CodeMemberField graphicsField = new CodeMemberField(typeof(System.Drawing.Graphics),"graphics");
			graphicsField.Attributes = MemberAttributes.Private;

			// constructor
			CodeConstructor defaultConstructor = new CodeConstructor();
			defaultConstructor.Attributes = MemberAttributes.Public;
			CodeMethodInvokeExpression initializeInvoke = new CodeMethodInvokeExpression(new CodeThisReferenceExpression(),"InitializeComponent",new CodeExpression[] {});			
			defaultConstructor.Statements.Add(initializeInvoke);			
			
			

			// dispose
			CodeMemberMethod disposeMethod = new CodeMemberMethod();
			disposeMethod.Name = "Dispose";
			disposeMethod.Attributes = MemberAttributes.Override | MemberAttributes.Public;
			disposeMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(bool),"disposing"));
			CodeSnippetStatement disposeInvoke = new CodeSnippetStatement("\tif( disposing )"+Environment.NewLine + "\t\t{"+Environment.NewLine + "\t\t\tif(components != null)"+Environment.NewLine + "\t\t\t{"+Environment.NewLine + "\t\t\t\tcomponents.Dispose();"+Environment.NewLine + "\t\t\t}"+Environment.NewLine + "\t\t}"+Environment.NewLine + "\t\tbase.Dispose( disposing );");
			disposeMethod.Statements.Add(disposeInvoke);

			//InitializeComponent
			CodeMemberMethod initializeComponentMethod = new CodeMemberMethod();
			initializeComponentMethod.Name = "InitializeComponent";
			CodeMethodInvokeExpression InitializeComponentInvoke = new CodeMethodInvokeExpression(new CodeThisReferenceExpression(),"ResumeLayout",new CodeExpression [] {new CodePrimitiveExpression(false)});			
            CodeSnippetStatement paintInvoke = new CodeSnippetStatement("this.Paint +=new PaintEventHandler(ToolControl_Paint);");			
			initializeComponentMethod.Statements.Add(paintInvoke);
			initializeComponentMethod.Statements.Add(InitializeComponentInvoke);

			//ToolControl_Paint			
			paintMethod.Name = "ToolControl_Paint";
			paintMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(object),"sender"));
			paintMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(System.Windows.Forms.PaintEventArgs),"e"));
			CodeSnippetExpression graphicsInitialize = new CodeSnippetExpression("graphics=this.CreateGraphics()");
			CodeSnippetExpression penInitialize = new CodeSnippetExpression("Pen pen");
			CodeSnippetExpression fontInitialize = new CodeSnippetExpression("Font font");
			paintMethod.Statements.Add(graphicsInitialize);
			paintMethod.Statements.Add(penInitialize);
			paintMethod.Statements.Add(fontInitialize);
			
			componentClass.Members.Add(componentsField);
			componentClass.Members.Add(graphicsField);			
			componentClass.Members.Add(defaultConstructor);
			componentClass.Members.Add(disposeMethod);
			componentClass.Members.Add(initializeComponentMethod);
			componentClass.Members.Add(paintMethod);			
			
			codeNamespace.Types.Add(componentClass);		
		}

		public void AddPaintExpressions(CodeExpression [] expressions)
		{
			foreach (CodeExpression expression in expressions)
			{
				paintMethod.Statements.Add(expression);
			}
		}


		public void Generate(string fileName)
		{			
			CodeDomProvider provider = new CSharpCodeProvider();
			ICodeGenerator gen = provider.CreateGenerator();		
			IndentedTextWriter tw = new IndentedTextWriter(new StreamWriter(fileName, false), "    ");
			gen.GenerateCodeFromCompileUnit(compileUnit, tw, new CodeGeneratorOptions());			
			tw.Close();			
		}
	}
}
