using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using PluginInterface;
namespace VectorConvertor.PluggableLogic
{
	/// <summary>
	/// Summary description for ToolManager.
	/// </summary>
	public class ToolManager
	{
		private ToolManager()
		{			
		}

		private static ToolManager fInstance;
		public static ToolManager Instance
		{
			get
			{
				if (fInstance == null)
					fInstance = new ToolManager();
				return fInstance;
			}
		}

		private  TabControl fParentTab;
		public  TabControl ParentTab
		{
			get
			{
				return fParentTab;
			}
			set
			{
				fParentTab = value;
			}
		}

		private Hashtable fPlugginToolbars = new Hashtable();
		

		public void LoadToolbarPlugins(string fileName, string toolBarName, VCCore core)
		{
			if (!fPlugginToolbars.ContainsKey(toolBarName))				
			{
				PluginCore.PluginSearch.Instance.AssemblyName = fileName;
				ArrayList keys = PluginCore.PluginSearch.Instance.GetPluginKeys();
				TabPage pluginTab = new TabPage(toolBarName);
				
				pluginTab.Name = "tbl"+toolBarName;
				ParentTab.TabPages.Add(pluginTab);
				int tabHeight = 10;
				foreach (object key in keys)
				{
					object plugin = PluginCore.PluginFactory.Instance.GetPlugin(key);				
					ToolControl toolControl = new ToolControl();
					toolControl.Image = ((BaseInterface)plugin).Presentation;				
					toolControl.ToolName = ((BaseInterface)plugin).Key.ToString();
					toolControl.Location = new System.Drawing.Point(0,tabHeight);				
					toolControl.PluginName = fileName;
					toolControl.ToolClickEvent +=new VectorConvertor.VCCore.ToolClick(toolControl_ToolClickEvent);
					toolControl.ToolClickEvent +=new VectorConvertor.VCCore.ToolClick(core.ToolClick_Event);
					tabHeight += toolControl.Width;
					pluginTab.Controls.Add(toolControl);
				}		
				fPlugginToolbars.Add(toolBarName,keys);
			}
		}		

		public bool ChangeToolState(string toolBarName, object key)
		{
			if (!fPlugginToolbars.ContainsKey(toolBarName))
				throw new InvalidProgramException("Toolbar " + toolBarName + " not loaded");
			ArrayList tools = (ArrayList)fPlugginToolbars[toolBarName];
			if (!tools.Contains(key))
				throw new InvalidProgramException("Toolbar " + toolBarName + " not have tool " + key.ToString());
			ToolControl toolControl = null;
			foreach (TabPage toolbar in ParentTab.TabPages)
			{
				if (toolbar.Name == "tbl"+toolBarName)
				{
					foreach (Control control in toolbar.Controls)
					{
						if (control is ToolControl && ((ToolControl)control).ToolName == key.ToString())
						{
							toolControl = (ToolControl)control;							
							break;
						}
					}
					if (toolControl != null)
					{					
						break;
					}					
				}				
			}
			if (toolControl == null)
				throw new InvalidProgramException("Toool " + key.ToString() + " not present in controls for " + toolBarName);

			foreach (TabPage toolbar in ParentTab.TabPages)
			{
				
				foreach (Control control in toolbar.Controls)
				{
					if (control is ToolControl)
					{
						if (control != toolControl)
                            ((ToolControl)control).Checked = false;						
					}
				}								
			}
			return toolControl.Checked;
		}		

		private void toolControl_ToolClickEvent(object sender)
		{			
			ToolControl toolControl = (ToolControl)sender;
			string toolbarName =((TabPage)toolControl.Parent).Name.Remove(0,3);
			ChangeToolState(toolbarName,toolControl.ToolName);
		}
	}
}
