using System;
using System.Collections;
using System.Drawing;
using System.CodeDom;
using VectorConvertorInterface;

namespace VectorConvertor.GraphEntity
{
	/// <summary>
	/// Summary description for CircleEntity.
	/// </summary>
	public class CircleEntity : IGraphEntity
	{
		public CircleEntity()
		{
		}

		public CircleEntity(Pen pen, ArrayList sequence)
		{
			if (pen == null)
				throw new ArgumentException("Pen is Empty");
			if (sequence == null || sequence.Count < 2)
				throw new ArgumentException("Sequence not have 2 points");

			fParameters = new Hashtable();
			fParameters.Add("Pen",pen);
			
			fPoints = new Hashtable();

			Size size = new Size(0,0);
			Point initPoint = new Point(0,0);

			if (((Point)sequence[sequence.Count - 1]).X > ((Point)sequence[sequence.Count - 2]).X && ((Point)sequence[sequence.Count - 1]).Y > ((Point)sequence[sequence.Count - 2]).Y)
			{
				initPoint = (Point)sequence[sequence.Count - 2];
				size = new Size(((Point)sequence[sequence.Count - 1]).X-((Point)sequence[sequence.Count - 2]).X,((Point)sequence[sequence.Count - 1]).Y-((Point)sequence[sequence.Count - 2]).Y);
			}

			if (((Point)sequence[sequence.Count - 1]).X < ((Point)sequence[sequence.Count - 2]).X && ((Point)sequence[sequence.Count - 1]).Y < ((Point)sequence[sequence.Count - 2]).Y)
			{
				initPoint = (Point)sequence[sequence.Count - 1];
				size = new Size(((Point)sequence[sequence.Count - 2]).X-((Point)sequence[sequence.Count - 1]).X,((Point)sequence[sequence.Count - 2]).Y-((Point)sequence[sequence.Count - 1]).Y);
			}

			if (((Point)sequence[sequence.Count - 1]).X > ((Point)sequence[sequence.Count - 2]).X && ((Point)sequence[sequence.Count - 1]).Y < ((Point)sequence[sequence.Count - 2]).Y)
			{
				initPoint = (Point)sequence[sequence.Count - 2];							
				size = new Size(((Point)sequence[sequence.Count - 1]).X-((Point)sequence[sequence.Count - 2]).X,((Point)sequence[sequence.Count - 2]).Y-((Point)sequence[sequence.Count - 1]).Y);
				initPoint.Offset(0,-size.Height);
			}

			if (((Point)sequence[sequence.Count - 1]).X < ((Point)sequence[sequence.Count - 2]).X && ((Point)sequence[sequence.Count - 1]).Y > ((Point)sequence[sequence.Count - 2]).Y)
			{
				initPoint = (Point)sequence[sequence.Count - 2];
				size = new Size(((Point)sequence[sequence.Count - 2]).X-((Point)sequence[sequence.Count - 1]).X,((Point)sequence[sequence.Count - 1]).Y-((Point)sequence[sequence.Count - 2]).Y);
				initPoint.Offset(-size.Width,0);
			}		

			fPoints.Add("Position",initPoint);
			fParameters.Add("Size",size);
		}

		#region IGraphEntity Members

		private string fName;
		public string Name
		{
			get
			{
				return fName;
			}

			set
			{
				fName = value;
			}
		}		

		private Hashtable fPoints = new Hashtable();
		public System.Collections.Hashtable Points
		{
			get
			{				
				return fPoints;
			}
			set
			{
				fPoints = value;
			}
		}

		private Hashtable fParameters = new Hashtable();
		public System.Collections.Hashtable Parameters
		{
			get
			{				
				return fParameters;
			}
			set
			{
				fParameters = value;
			}
		}

		public void Print(Graphics graphics)
		{
			Pen pen = (Pen)fParameters["Pen"];
			Size size = (Size)fParameters["Size"];
			Point position = (Point)fPoints["Position"];
			if (fSelected)
			{
				Pen selectedPen = (Pen)pen.Clone();
				selectedPen.Color = System.Drawing.Color.Gray;
				graphics.DrawEllipse(selectedPen,position.X,position.Y,size.Width,size.Height);
			}
			else
				graphics.DrawEllipse(pen,position.X,position.Y,size.Width,size.Height);
		}

		public CodeExpression [] GetInvokation()
		{
			Pen pen = (Pen)fParameters["Pen"];			
			Size size = (Size)fParameters["Size"];
			Point position = (Point)fPoints["Position"];
			ArrayList expressions = new ArrayList();
            
			CodeFieldReferenceExpression graphicsReference = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(),"graphics");			
			CodeObjectCreateExpression newPenObject = new CodeObjectCreateExpression(typeof(System.Drawing.Pen),new CodeExpression [] {new CodeSnippetExpression("Color.FromName(\""+pen.Color.Name + "\")"),new CodePrimitiveExpression(pen.Width)});
			CodeVariableReferenceExpression penObject = new CodeVariableReferenceExpression("pen");
			
			CodeSnippetExpression penCreate = new CodeSnippetExpression("pen = new Pen("+"Color.FromArgb("+pen.Color.R + ", " + pen.Color.G +", " + pen.Color.B + "), " + pen.Width.ToString()+ ")");			

			//graphics.DrawEllipse(pen,position.X,position.Y,size.Width,size.Height);
			CodeMethodInvokeExpression drawCircle = new CodeMethodInvokeExpression(graphicsReference,"DrawEllipse", new CodeExpression [] {penObject,new CodePrimitiveExpression(position.X), new CodePrimitiveExpression(position.Y), new CodePrimitiveExpression(size.Width), new CodePrimitiveExpression(size.Height)});

			expressions.Add(new CodeSnippetExpression("//Circle " + fName));
			expressions.Add(penCreate);
			expressions.Add(drawCircle);		

			return (CodeExpression [])expressions.ToArray(typeof(CodeExpression));
		}

		private bool fSelected;
		public bool Selected
		{
			get
			{
				return fSelected;
			}
			set
			{
				fSelected = value;
			}
		}

		public int PointsCountComplite
		{
			get
			{
				return 2;
			}
			set
			{

			}
		}

		public void SetPointSequence(ArrayList sequence)
		{
			if (sequence == null || sequence.Count < 2)
				throw new ArgumentException("Sequence not have 2 points");
			
			fPoints = new Hashtable();

			Size size = new Size(0,0);
			Point initPoint = new Point(0,0);

			if (((Point)sequence[sequence.Count - 1]).X > ((Point)sequence[sequence.Count - 2]).X && ((Point)sequence[sequence.Count - 1]).Y > ((Point)sequence[sequence.Count - 2]).Y)
			{
				initPoint = (Point)sequence[sequence.Count - 2];
				size = new Size(((Point)sequence[sequence.Count - 1]).X-((Point)sequence[sequence.Count - 2]).X,((Point)sequence[sequence.Count - 1]).Y-((Point)sequence[sequence.Count - 2]).Y);
			}

			if (((Point)sequence[sequence.Count - 1]).X < ((Point)sequence[sequence.Count - 2]).X && ((Point)sequence[sequence.Count - 1]).Y < ((Point)sequence[sequence.Count - 2]).Y)
			{
				initPoint = (Point)sequence[sequence.Count - 1];
				size = new Size(((Point)sequence[sequence.Count - 2]).X-((Point)sequence[sequence.Count - 1]).X,((Point)sequence[sequence.Count - 2]).Y-((Point)sequence[sequence.Count - 1]).Y);
			}

			if (((Point)sequence[sequence.Count - 1]).X > ((Point)sequence[sequence.Count - 2]).X && ((Point)sequence[sequence.Count - 1]).Y < ((Point)sequence[sequence.Count - 2]).Y)
			{
				initPoint = (Point)sequence[sequence.Count - 2];							
				size = new Size(((Point)sequence[sequence.Count - 1]).X-((Point)sequence[sequence.Count - 2]).X,((Point)sequence[sequence.Count - 2]).Y-((Point)sequence[sequence.Count - 1]).Y);
				initPoint.Offset(0,-size.Height);
			}

			if (((Point)sequence[sequence.Count - 1]).X < ((Point)sequence[sequence.Count - 2]).X && ((Point)sequence[sequence.Count - 1]).Y > ((Point)sequence[sequence.Count - 2]).Y)
			{
				initPoint = (Point)sequence[sequence.Count - 2];
				size = new Size(((Point)sequence[sequence.Count - 2]).X-((Point)sequence[sequence.Count - 1]).X,((Point)sequence[sequence.Count - 1]).Y-((Point)sequence[sequence.Count - 2]).Y);
				initPoint.Offset(-size.Width,0);
			}		

			fPoints.Add("Position",initPoint);
			fParameters.Add("Size",size);
		}


		#endregion

		#region BaseInterface Members
		
		public bool Identify(object key)
		{
			return Key.Equals(key);
		}
		public object Key
		{
			get
			{
				return "Circle";
			}
			set
			{
			}
		}	

		public Bitmap Presentation
		{
			get
			{
				return PresentationManager.Instance.GetPresentation("CIRC.BMP");
			}
			set
			{
			}
		}
		
		#endregion
	}
}
