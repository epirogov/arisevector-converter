using System;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;
using System.CodeDom;
using VectorConvertorInterface;

namespace VectorConvertor.GraphEntity
{
	/// <summary>
	/// Summary description for TextEntity.
	/// </summary>
	public class TextEntity : IGraphEntity
	{
		public TextEntity()
		{			
		}

		public TextEntity(string text, Font font, Pen pen, ArrayList sequence)
		{
			if (font == null)
				throw new ArgumentException("Font is empty");
			if (pen == null)
				throw new ArgumentException("Pen is empty");
			if (sequence == null || sequence.Count == 0)
				throw new ArgumentException("Sequence empty");

			fParameters = new Hashtable();
			fPoints = new Hashtable();
			
			fParameters.Add("Font",font);
			fParameters.Add("Pen",pen);
			fParameters.Add("Text",text);

			Point point = (Point)sequence[sequence.Count - 1];
			fPoints.Add("Position",point);
		}

		public Font TextFont
		{
			get
			{
				if (fParameters.ContainsKey("Font"))
					return (Font)fParameters["Font"];
				else
					return null;
			}
			set
			{
				if (fParameters.ContainsKey("Font"))
					fParameters["Font"] = value;
				else
					fParameters.Add("Font",value);
			}
		}

		public string Text
		{
			get
			{
				if (fParameters.ContainsKey("Text"))
					return (string)fParameters["Text"];
				else
					return null;
			}
			set
			{
				if (fParameters.ContainsKey("text"))
					fParameters["Text"] = value;
				else
					fParameters.Add("Text",value);
			}
		}

		#region IGraphEntity Members

		private string fName;
		public string Name
		{
			get
			{
				return fName;
			}

			set
			{
				fName = value;
			}
		}

		private Hashtable fPoints = new Hashtable();
		public System.Collections.Hashtable Points
		{
			get
			{				
				return fPoints;
			}
			set
			{
				fPoints = value;
			}
		}

		private Hashtable fParameters = new Hashtable();
		public System.Collections.Hashtable Parameters
		{
			get
			{				
				return fParameters;
			}
			set
			{
				fParameters = value;
			}
		}

		public void Print(Graphics graphics)
		{			
			string text = (string)fParameters["Text"];
			Font font = (Font)fParameters["Font"];
			Brush brush = ((Pen)fParameters["Pen"]).Brush;
			Point position = (Point)fPoints["Position"];
			Pen pen = (Pen)fParameters["Pen"];
			if (fSelected)
			{
				Pen selectedPen = (Pen)pen.Clone();
				selectedPen.Color = System.Drawing.Color.Gray;				
				brush = selectedPen.Brush;
			}                
			graphics.DrawString(text,font,brush,position.X,position.Y);
		}

		public CodeExpression [] GetInvokation()
		{
			string text = (string)fParameters["Text"];
			Font font = (Font)fParameters["Font"];
			Brush brush = ((Pen)fParameters["Pen"]).Brush;			
			Pen pen = (Pen)fParameters["Pen"];
			Point position = (Point)fPoints["Position"];			
			
			ArrayList expressions = new ArrayList();			
			
			CodeFieldReferenceExpression graphicsReference = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(),"graphics");			
			CodeObjectCreateExpression newPenObject = new CodeObjectCreateExpression(typeof(System.Drawing.Pen),new CodeExpression [] {new CodeSnippetExpression("Color.FromName(\""+pen.Color.Name + "\")"),new CodePrimitiveExpression(pen.Width)});
			CodeVariableReferenceExpression penObject = new CodeVariableReferenceExpression("pen");
			
			CodeSnippetExpression penCreate = new CodeSnippetExpression("pen = new Pen("+"Color.FromArgb("+pen.Color.R + ", " + pen.Color.G +", " + pen.Color.B + "), " + pen.Width.ToString()+ ")");			
			
			CodeSnippetExpression fontCreate = new CodeSnippetExpression("font = new Font(\""+font.Name + "\", " + font.Size.ToString() + ")");			

			//graphics.DrawString(text,font,brush,position.X,position.Y);
			CodeMethodInvokeExpression drawText = new CodeMethodInvokeExpression(graphicsReference,"DrawString", new CodeExpression [] {new CodePrimitiveExpression(text), new CodeVariableReferenceExpression("font"), new CodePropertyReferenceExpression(penObject,"Brush"),new CodePrimitiveExpression(position.X), new CodePrimitiveExpression(position.Y)});

			expressions.Add(new CodeSnippetExpression("//Text " + fName));
			expressions.Add(penCreate);
			expressions.Add(fontCreate);
			expressions.Add(drawText);		

			return (CodeExpression [])expressions.ToArray(typeof(CodeExpression));
		}

		private bool fSelected;
		public bool Selected
		{
			get
			{
				return fSelected;
			}
			set
			{
				fSelected = value;
			}
		}

		public int PointsCountComplite
		{
			get
			{
				return 1;
			}
			set
			{

			}
		}

		public void SetPointSequence(ArrayList sequence)
		{
			TextEntityCreatorForm textEntityCreatorForm = new TextEntityCreatorForm();
			if (textEntityCreatorForm.ShowDialog() == DialogResult.OK)
			{
				textEntityCreatorForm.SetTextEntityParams(this);
			}
			else
				throw new CancelActionException("TextEntity canceled by user");		

			if (sequence == null || sequence.Count == 0)
				throw new ArgumentException("Sequence empty");
			
			fPoints = new Hashtable();			

			Point point = (Point)sequence[sequence.Count - 1];
			fPoints.Add("Position",point);
		}

		#endregion

		#region BaseInterface Members
		
		public bool Identify(object key)
		{
			return Key.Equals(key);
		}
		public object Key
		{
			get
			{
				return "Text";
			}
			set
			{
			}
		}	

		public Bitmap Presentation
		{
			get
			{
				return PresentationManager.Instance.GetPresentation("TEXT.BMP");
			}
			set
			{
			}
		}
		
		#endregion
	}
}
