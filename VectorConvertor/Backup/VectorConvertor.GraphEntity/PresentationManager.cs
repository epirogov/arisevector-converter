using System;
using System.Drawing;
using System.Resources;

namespace VectorConvertor.GraphEntity
{
	/// <summary>
	/// Summary description for PresentationManager.
	/// </summary>
	public class PresentationManager
	{
		private ResourceManager fResource;
		private PresentationManager()
		{
			fResource = new ResourceManager("VectorConvertor.GraphEntity.toolbarIcons",typeof(PresentationManager).Assembly);
		}
		private static PresentationManager fInstance;
		public static PresentationManager Instance
		{
			get
			{
				if (fInstance == null)
					fInstance = new PresentationManager();
				return fInstance;
			}
		}

		public Bitmap GetPresentation(string key)
		{
			return (Bitmap)fResource.GetObject(key);
		}
	}
}
