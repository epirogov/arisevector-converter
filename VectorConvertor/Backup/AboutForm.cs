using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace VectorConvertor
{
	/// <summary>
	/// Summary description for AboutForm.
	/// </summary>
	public class AboutForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbVersion;
		private VectorConvertor.VCLogo vcLogo1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AboutForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AboutForm));
			this.label1 = new System.Windows.Forms.Label();
			this.lbVersion = new System.Windows.Forms.Label();
			this.vcLogo1 = new VectorConvertor.VCLogo();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(136, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Version :";
			// 
			// lbVersion
			// 
			this.lbVersion.Location = new System.Drawing.Point(208, 48);
			this.lbVersion.Name = "lbVersion";
			this.lbVersion.Size = new System.Drawing.Size(160, 16);
			this.lbVersion.TabIndex = 1;
			this.lbVersion.Text = "[Version]";
			// 
			// vcLogo1
			// 
			this.vcLogo1.Location = new System.Drawing.Point(-8, -16);
			this.vcLogo1.Name = "vcLogo1";
			this.vcLogo1.Size = new System.Drawing.Size(384, 128);
			this.vcLogo1.TabIndex = 2;
			// 
			// AboutForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(376, 77);
			this.Controls.Add(this.lbVersion);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.vcLogo1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "AboutForm";
			this.Opacity = 0.9;
			this.Text = "AboutForm";
			this.Load += new System.EventHandler(this.AboutForm_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void AboutForm_Load(object sender, System.EventArgs e)
		{
			lbVersion.Text = Application.ProductVersion;
		}
	}
}
