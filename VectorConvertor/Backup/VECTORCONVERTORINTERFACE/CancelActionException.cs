using System;
using System.Runtime.Serialization;

namespace VectorConvertorInterface
{
	/// <summary>
	/// Summary description for CancelActionException.
	/// </summary>
	public class CancelActionException : Exception, ISerializable
	{
		public CancelActionException(string message) : base (message)
		{			
		}

		public CancelActionException()
		{			
		}
	}
}
