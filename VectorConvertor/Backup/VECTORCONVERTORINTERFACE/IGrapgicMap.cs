using System;
using System.Windows.Forms;

namespace VectorConvertorInterface
{
	/// <summary>
	/// Summary description for IGrapgicMap.
	/// </summary>
	public interface IGrapgicMap
	{
		string ToolName
		{
			get;
			set;
		}

		object SelectedObject
		{
			get;
			set;
		}		

		Control Map
		{
			get;			
			set;			
		}

		ContextMenu MapMenu
		{
			get;
			set;
		}
	}
}
