using System;
using System.Drawing;
using System.Collections;
using System.CodeDom;

namespace VectorConvertorInterface
{
	/// <summary>
	/// Summary description for IGraphEntity.
	/// </summary>
	public interface IGraphEntity : PluginInterface.BaseInterface
	{
		string Name
		{
			get;
			set;
		}		

		Hashtable Points
		{
			get;			
			set;
		}

		int PointsCountComplite
		{
			get;
			set;
		}

		Hashtable Parameters
		{
			get;
			set;
		}

		void Print(Graphics graphics);		
		bool Selected
		{
			get;
			set;
		}

		CodeExpression [] GetInvokation();		

		void SetPointSequence(ArrayList sequence);
	}
}
