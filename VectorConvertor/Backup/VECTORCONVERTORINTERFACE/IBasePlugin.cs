using System;
using System.Drawing;

namespace PluginInterface
{
	public interface BaseInterface
	{
		bool Identify(object key);
		object Key
		{
			get;
			set;
		}	
	
		Bitmap Presentation
		{
			get;
			set;
		}
	}
}