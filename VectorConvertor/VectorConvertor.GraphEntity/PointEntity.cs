using System;
using System.Collections;
using System.Drawing;
using System.CodeDom;
using VectorConvertorInterface;

namespace VectorConvertor.GraphEntity
{
	/// <summary>
	/// Summary description for PointEntity.
	/// </summary>
	public class PointEntity : IGraphEntity
	{
		public PointEntity()
		{
		}

		public PointEntity(Pen pen, ArrayList sequence)
		{
			if (pen == null)
				throw new ArgumentException("Pen is Empty");
			if (sequence == null || sequence.Count == 0)
				throw new ArgumentException("Sequence empty");

			fParameters = new Hashtable();
			fParameters.Add("Pen",pen);
			
			fPoints = new Hashtable();

			Point point = (Point)sequence[sequence.Count - 1];
			fPoints.Add("Position",point);
		}
		#region IGraphEntity Members

		private string fName;
		public string Name
		{
			get
			{
				return fName;
			}

			set
			{
				fName = value;
			}
		}

		private Hashtable fPoints = new Hashtable();
		public System.Collections.Hashtable Points
		{
			get
			{				
				return fPoints;
			}
			set
			{
				fPoints = value;
			}
		}

		private Hashtable fParameters = new Hashtable();
		public System.Collections.Hashtable Parameters
		{
			get
			{				
				return fParameters;
			}
			set
			{
				fParameters = value;
			}
		}

		public void Print(Graphics graphics)
		{
			Pen pen = (Pen)fParameters["Pen"];
			Point position = (Point)fPoints["Position"];
			if (fSelected)
			{
				Pen selectedPen = (Pen)pen.Clone();
				selectedPen.Color = System.Drawing.Color.Gray;
				graphics.DrawEllipse(selectedPen,position.X,position.Y,1,1);
			}
			else
				graphics.DrawEllipse(pen,position.X,position.Y,1,1);
		}

		public CodeExpression [] GetInvokation()
		{
			Pen pen = (Pen)fParameters["Pen"];
			Point position = (Point)fPoints["Position"];
			ArrayList expressions = new ArrayList();						
            
			CodeFieldReferenceExpression graphicsReference = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(),"graphics");			
			CodeObjectCreateExpression newPenObject = new CodeObjectCreateExpression(typeof(System.Drawing.Pen),new CodeExpression [] {new CodeSnippetExpression("Color.FromName(\""+pen.Color.Name + "\")"),new CodePrimitiveExpression(pen.Width)});
			CodeVariableReferenceExpression penObject = new CodeVariableReferenceExpression("pen");
			
			CodeSnippetExpression penCreate = new CodeSnippetExpression("pen = new Pen("+"Color.FromArgb("+pen.Color.R + ", " + pen.Color.G +", " + pen.Color.B + "), " + pen.Width.ToString()+ ")");			

			//graphics.DrawEllipse(pen,position.X,position.Y,1,1);
			CodeMethodInvokeExpression drawPoint = new CodeMethodInvokeExpression(graphicsReference,"DrawEllipse", new CodeExpression [] {penObject,new CodePrimitiveExpression(position.X), new CodePrimitiveExpression(position.Y), new CodePrimitiveExpression(1), new CodePrimitiveExpression(1)});

			expressions.Add(new CodeSnippetExpression("//Point " + fName));			
			expressions.Add(penCreate);
			expressions.Add(drawPoint);		

			return (CodeExpression [])expressions.ToArray(typeof(CodeExpression));
		}

		private bool fSelected;
		public bool Selected
		{
			get
			{
				return fSelected;
			}
			set
			{
				fSelected = value;
			}
		}

		public int PointsCountComplite
		{
			get
			{
				return 1;
			}
			set
			{

			}
		}

		public void SetPointSequence(ArrayList sequence)
		{
			if (sequence == null || sequence.Count == 0)
				throw new ArgumentException("Sequence empty");
			
			fPoints = new Hashtable();

			Point point = (Point)sequence[sequence.Count - 1];
			fPoints.Add("Position",point);	
		}

		#endregion

		#region BaseInterface Members
		
		public bool Identify(object key)
		{
			return Key.Equals(key);
		}
		public object Key
		{
			get
			{
				return "Point";
			}
			set
			{
			}
		}	

		public Bitmap Presentation
		{
			get
			{
				return PresentationManager.Instance.GetPresentation("POINT.BMP");
			}
			set
			{
			}
		}
		
		#endregion
	}
}
