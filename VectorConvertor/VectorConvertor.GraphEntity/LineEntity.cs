using System;
using System.Collections;
using System.Drawing;
using System.CodeDom;
using VectorConvertorInterface;

namespace VectorConvertor.GraphEntity
{
	/// <summary>
	/// Summary description for LineEntity.
	/// </summary>
	public class LineEntity : IGraphEntity
	{
		public LineEntity()
		{
		}

		public LineEntity(Pen pen, ArrayList sequence)
		{
			if (pen == null)
				throw new ArgumentException("Pen is Empty");
			if (sequence == null || sequence.Count < 2)
				throw new ArgumentException("Sequence no have 2 points");

			fParameters = new Hashtable();
			fParameters.Add("Pen",pen);
			
			fPoints = new Hashtable();

			Point positionFirst = (Point)sequence[sequence.Count - 2];
			Point positionLast = (Point)sequence[sequence.Count - 1];
			fPoints.Add("PositionFirst",positionFirst);
			fPoints.Add("PositionLast",positionLast);
		}
		#region IGraphEntity Members

		private string fName;
		public string Name
		{
			get
			{
				return fName;
			}

			set
			{
				fName = value;
			}
		}

		private Hashtable fPoints = new Hashtable();
		public System.Collections.Hashtable Points
		{
			get
			{				
				return fPoints;
			}
			set
			{
				fPoints = value;
			}
		}

		private Hashtable fParameters = new Hashtable();
		public System.Collections.Hashtable Parameters
		{
			get
			{				
				return fParameters;
			}
			set
			{
				fParameters = value;
			}
		}

		public void Print(Graphics graphics)
		{
			Pen pen = (Pen)fParameters["Pen"];
			Point positionFirst = (Point)fPoints["PositionFirst"];
			Point positionLast = (Point)fPoints["PositionLast"];
			if (fSelected)
			{
				Pen selectedPen = (Pen)pen.Clone();
				selectedPen.Color = System.Drawing.Color.Gray;
				graphics.DrawLine(selectedPen,positionFirst,positionLast);	
			}
			else
				graphics.DrawLine(pen,positionFirst,positionLast);			
		}

		public CodeExpression [] GetInvokation()
		{
			Pen pen = (Pen)fParameters["Pen"];
			Point positionFirst = (Point)fPoints["PositionFirst"];
			Point positionLast = (Point)fPoints["PositionLast"];			
			ArrayList expressions = new ArrayList();			

			CodeFieldReferenceExpression graphicsReference = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(),"graphics");			
			CodeObjectCreateExpression newPenObject = new CodeObjectCreateExpression(typeof(System.Drawing.Pen),new CodeExpression [] {new CodeSnippetExpression("Color.FromName(\""+pen.Color.Name + "\")"),new CodePrimitiveExpression(pen.Width)});
			CodeVariableReferenceExpression penObject = new CodeVariableReferenceExpression("pen");			

			CodeSnippetExpression penCreate = new CodeSnippetExpression("pen = new Pen("+"Color.FromArgb("+pen.Color.R + ", " + pen.Color.G +", " + pen.Color.B + "), " + pen.Width.ToString()+ ")");			

			//graphics.DrawLine(pen,x1,y1,x2,y2);			
			CodeMethodInvokeExpression drawLine = new CodeMethodInvokeExpression(graphicsReference,"DrawLine", new CodeExpression [] {penObject,new CodePrimitiveExpression(positionFirst.X), new CodePrimitiveExpression(positionFirst.Y), new CodePrimitiveExpression(positionLast.X), new CodePrimitiveExpression(positionLast.Y)});

			expressions.Add(new CodeSnippetExpression("//Line " + fName));			
			expressions.Add(penCreate);
			expressions.Add(drawLine);

			return (CodeExpression [])expressions.ToArray(typeof(CodeExpression));
		}

		private bool fSelected;
		public bool Selected
		{
			get
			{
				return fSelected;
			}
			set
			{
				fSelected = value;
			}
		}

		public int PointsCountComplite
		{
			get
			{
				return 2;
			}
			set
			{

			}
		}

		public void SetPointSequence(ArrayList sequence)
		{			
			if (sequence == null || sequence.Count < 2)
				throw new ArgumentException("Sequence no have 2 points");			
			
			fPoints = new Hashtable();

			Point positionFirst = (Point)sequence[sequence.Count - 2];
			Point positionLast = (Point)sequence[sequence.Count - 1];
			fPoints.Add("PositionFirst",positionFirst);
			fPoints.Add("PositionLast",positionLast);
		}

		#endregion

		#region BaseInterface Members
		
		public bool Identify(object key)
		{
			return Key.Equals(key);
		}
		public object Key
		{
			get
			{
				return "Line";
			}
			set
			{
			}
		}
	
		public Bitmap Presentation
		{
			get
			{
				return PresentationManager.Instance.GetPresentation("LINE.BMP");
			}
			set
			{
			}
		}
		
		#endregion
	}
}
