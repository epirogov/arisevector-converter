using System;
using System.Collections;
using System.Drawing;
using VectorConvertorInterface;

namespace VectorConvertor.MoveLogic
{
	/// <summary>
	/// Summary description for MathGeometry.
	/// </summary>
	public sealed class MathGeometry
	{
		private MathGeometry()
		{		
		}

		public static Point GetCentre(IGraphEntity entity)
		{
			Point centre = new Point(0,0);
			if (!(entity.Parameters.Contains("Size") || entity.Points.Contains("Position")))
			{
				foreach (string key in entity.Points.Keys)
				{
					centre.X += ((Point)entity.Points[key]).X;
					centre.Y += ((Point)entity.Points[key]).Y;
				}

				centre.X = centre.X / entity.Points.Count;
				centre.Y = centre.Y / entity.Points.Count;
			}
			else
			{
				centre = (Point)entity.Points["Position"];
				if (entity.Parameters.Contains("Size"))
				{
					centre.X = centre.X + ((Size)entity.Parameters["Size"]).Width /2;
					centre.Y = centre.Y + ((Size)entity.Parameters["Size"]).Height /2;
				}
			}
			return centre;
		}

		public static void SetNewCentre(IGraphEntity entity, Point centre)
		{
			Point oldCentre = GetCentre(entity);
			ArrayList keys = new ArrayList();
			foreach (string key in entity.Points.Keys)
			{
				keys.Add(key);
			}

			foreach (string key in keys)
			{
				Point newPoint = new Point(0,0);
				newPoint.X = (((Point)entity.Points[key]).X - oldCentre.X + centre.X);
				newPoint.Y = (((Point)entity.Points[key]).Y - oldCentre.Y + centre.Y);
				entity.Points[key] = newPoint;
			}
			
		}

		public static double GetDistance(Point pointOne, Point pointTwo)
		{
			return Math.Pow(Math.Pow(pointOne.X - pointTwo.X,2) + Math.Pow(pointOne.Y - pointTwo.Y,2),0.5);
		}

		public static Point GetMaxDistantPoint(IGraphEntity entity, Point centre)
		{			
			Point maxDistPoint = new Point(0,0);
			double maxDist = double.MaxValue;
			foreach (string key in entity.Points.Keys)
			{
				if (maxDist > GetDistance((Point)entity.Points[key],centre))
				{
					maxDist = GetDistance((Point)entity.Points[key],centre);
					maxDistPoint = (Point)entity.Points[key];
				}

			}

			return maxDistPoint;
		}

		public static Point ScalePoint(Point scaledPoint, Point maxDistantPoint, Point centre, Point actionPoint)
		{
			Point newPoint = new Point(0,0);			
            
			double oldDistance = GetDistance(scaledPoint, centre);
			double maxDistance = GetDistance(maxDistantPoint, centre);
			double newMaxDistance = GetDistance(actionPoint, centre);
			double newDistance = oldDistance * newMaxDistance / maxDistance;			
			double cos = (scaledPoint.X - centre.X)/oldDistance;
			double sin = (scaledPoint.Y - centre.Y)/oldDistance;

			newPoint.X = Convert.ToInt32( (newDistance)* cos) + centre.X;
			newPoint.Y = Convert.ToInt32( (newDistance) * sin) + centre.Y;			

			return newPoint;			
		}

		public static Point RotatePoint(Point rotatedPoint, Point centre, Point actionPoint)
		{
			Point newPoint = new Point(0,0);
            
			double rotatedDistance = GetDistance(rotatedPoint, centre);			
			double rotateDistance = GetDistance(actionPoint, centre);			

			double cos = (rotatedPoint.X - centre.X)/rotatedDistance;
			double sin = (rotatedPoint.Y - centre.Y)/rotatedDistance;
			double cosAdded = (actionPoint.X - centre.X)/rotateDistance;
			double sinAdded = (actionPoint.Y - centre.Y)/rotateDistance;

			newPoint.X = Convert.ToInt32( (rotatedDistance)* (cos*cosAdded -  sin*sinAdded)) + centre.X;
			newPoint.Y = Convert.ToInt32( (rotatedDistance) * (sin*cosAdded + cos*sinAdded)) + centre.Y;
			
			return newPoint;			
		}

		public static void ScaleEntity(IGraphEntity entity, Point actionPoint)
		{
			Point centre = GetCentre(entity);			
			Point maxDistancePoint;
			if (!entity.Points.Contains("Position"))
			{
				ArrayList keys = new ArrayList();
				foreach (string key in entity.Points.Keys)
				{
					keys.Add(key);
				}

				maxDistancePoint = GetMaxDistantPoint(entity,centre);
				foreach (string key in keys)
				{
					Point newPoint = ScalePoint((Point)entity.Points[key],maxDistancePoint,centre,actionPoint);				
					entity.Points[key] = newPoint;
				}
			}
			else
			{
				if (entity.Parameters.Contains("Size"))
				{
					maxDistancePoint = (Point)entity.Points["Position"];
					maxDistancePoint.X += ((Size)entity.Parameters["Size"]).Width;
					maxDistancePoint.Y += ((Size)entity.Parameters["Size"]).Height;				
				
					entity.Points["Position"] = ScalePoint((Point)entity.Points["Position"],maxDistancePoint,centre,actionPoint);
					maxDistancePoint = ScalePoint(maxDistancePoint,maxDistancePoint,centre,actionPoint);
					entity.Parameters["Size"] = new Size(maxDistancePoint.X - ((Point)entity.Points["Position"]).X, maxDistancePoint.Y - ((Point)entity.Points["Position"]).Y);
				}
			}
		}

		public static void RotateEntity(IGraphEntity entity, Point actionPoint)
		{
			Point centre = GetCentre(entity);			
			if (!entity.Points.Contains("Position"))
			{
				ArrayList keys = new ArrayList();
				foreach (string key in entity.Points.Keys)
				{
					keys.Add(key);
				}
				
				foreach (string key in keys)
				{
					Point newPoint = RotatePoint((Point)entity.Points[key],centre,actionPoint);				
					entity.Points[key] = newPoint;
				}
			}
		}
	}
}
