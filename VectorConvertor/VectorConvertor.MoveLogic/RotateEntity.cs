using System;
using System.Drawing;
using System.Drawing;
using VectorConvertorInterface;

namespace VectorConvertor.MoveLogic
{
	/// <summary>
	/// Summary description for RotateEntity.
	/// </summary>
	public class RotateEntity : IMoveEntity
	{
		private RotateEntity()
		{			
		}

		private static RotateEntity fInstance;
		public static RotateEntity Instance
		{
			get
			{
				if(fInstance == null)
					fInstance = new RotateEntity();
				return fInstance;
			}
		}

		#region IMoveEntity Members

		private System.Collections.Hashtable fParameters;
		public System.Collections.Hashtable Parameters
		{
			get
			{				
				return fParameters;
			}
			set
			{
				fParameters = value;
			}
		}

		private System.Collections.Hashtable fPoints;
		public System.Collections.Hashtable Points
		{
			get
			{				
				return fPoints;
			}
			set
			{
				fPoints = value;
			}
		}

		public void Create(IGraphEntity entity, System.Collections.Hashtable parameters)
		{
			if (!parameters.Contains("Position"))
				throw new ArgumentException("Parameters not contain key 'Position'");
			fParameters = parameters;
			fGraphEntity = entity;
		}


		private IGraphEntity fGraphEntity;
		public IGraphEntity GraphEntity
		{
			get
			{
				return fGraphEntity;
			}
			set
			{
				fGraphEntity = value;
			}
		}

		public void Execute()
		{
			MathGeometry.RotateEntity(fGraphEntity,((System.Drawing.Point)fParameters["Position"]));
		}

		#endregion

		#region BaseInterface Members
		
		public bool Identify(object key)
		{
			return Key.Equals(key);
		}
		public object Key
		{
			get
			{
				return "Rotate Entity";
			}
			set
			{
			}
		}
	
		public Bitmap Presentation
		{
			get
			{
				return PresentationManager.Instance.GetPresentation("RECT.BMP");
			}
			set
			{
			}
		}
		
		#endregion
	}
}
